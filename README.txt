Readme
------

This is a module that gives users a formatting bar that inserts BBCode into posts using Javascript.  Works on IE and Firefox.

Installation
------------

1. Copy the files into a bbcode_wysiwyg folder under the Drupal modules/ directory.

2. Go to the administration section, into "adminster > modules"
   and enable the bbcode_wysiwyg module.

Author
------

Allen Cook <pyro@gamerswithjobs.com>

Ported to Drupal 4.7 and 5.x by Alan Harder <alan.harder@sun.com>

