// Javascript for the PM Quick Reply MOD by Rondom <rondom@arcor.de> http://www.rondom.gu2.info
// Based on Editor Mod by Smartor <smartor_xp@hotmail.com> http://smartor.is-root.com
// and Advanced Quick Reply by Rusty Dragon <dev@RustyDragon.com>
// Don't remove these comments!

var clientPC = navigator.userAgent.toLowerCase();
var clientVer = parseInt(navigator.appVersion);

var is_ie = ((clientPC.indexOf("msie") != -1) && (clientPC.indexOf("opera") == -1));
var is_clip_able = ((clientPC.indexOf("msie") != -1));
var is_nav  = ((clientPC.indexOf('mozilla')!=-1) && (clientPC.indexOf('spoofer')==-1)
                && (clientPC.indexOf('compatible') == -1) && (clientPC.indexOf('opera')==-1)
                && (clientPC.indexOf('webtv')==-1) && (clientPC.indexOf('hotjava')==-1));

var is_win   = ((clientPC.indexOf("win")!=-1) || (clientPC.indexOf("16bit") != -1));
var is_mac    = (clientPC.indexOf("mac")!=-1);
var bb_open = new Array(3);

if (window.Event){
  document.captureEvents(Event.KEYDOWN);
  document.captureEvents(Event.KEYUP);
}
document.onkeydown = myKeyDown;
document.onkeyup = myKeyUp;

var ctrl_pressed = false;
var shift_pressed = false;

function getTextarea()  {
  var textarea;
  var areas = document.getElementsByTagName("textarea");
  for (var i=0; i<areas.length; i++)  {
    if (areas[i].id.match("edit"))  {
      textarea = areas[i];
      break;
    }
  }
  return textarea;
}

function getTextareaSelection()  {
  var sel;
  if (is_ie)  {
    var range = document.selection.createRange();
    sel = range.text; // Get text selection
  } else {
    sel = window.getSelection(); // Get text selection
    if (sel == "") {
      var textarea = getTextarea();
      sel = textarea.value.substr(textarea.selectionStart,textarea.selectionEnd-textarea.selectionStart);
    }
  }
  return sel;
}

function quoteSelection() {
  var textarea = getTextarea();
  var theSelection = getTextareaSelection();
  if (theSelection == '')  {
    if (bb_open[2])  {
      bb_open[2] = false;
      PostWrite('[/quote]');
    } else {
      bb_open[2] = true;
      PostWrite('[quote]');
    }
  } else {
    // Add tags around selection
    PostWrite( '[quote]' + theSelection + '[/quote]');
  }
}

function setClicked(id)  {
  if (id == 'bbbold') var bbcid=0;
  if (id == 'bbitalic') var bbcid=1;
  if (id == 'bbquote') var bbcid=2;
  if (id == 'bbcenter') var bbcid=3;
  if (bb_open[bbcid])
    document.getElementById(id).className = 'bbcode_wysiwyg_down';
  else
    document.getElementById(id).className = 'bbcode_wysiwyg';
}

function emoticon(text) {
  text = ' ' + text + ' ';
  PostWrite(text);
}

function bbfontstyle(bbopen, bbclose) {
  var textarea = getTextarea();
  var theSelection = getTextareaSelection();
  if (!theSelection) {
    textarea.value += bbopen + bbclose;
    textarea.focus();
    return;
  }
  PostWrite(bbopen + theSelection + bbclose);
  textarea.focus();
}

function storeCaret(textEl) {
  if (is_ie)  {
    if (!textEl) textEl = getTextarea();
    if (textEl.createTextRange) 
      textEl.caretPos = document.selection.createRange().duplicate();
  }
}

function PostWrite(text) {
  var textarea = getTextarea();
  if (is_ie) {
    if (textarea.caretPos)  {
      var caretPos = textarea.caretPos;
      caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? text + ' ' : text;
      caretPos.collapse(false);
      textarea.focus(caretPos);
    } else insertAtCursor(textarea,text);
    return;
  } else if (textarea.setSelectionRange) {
     var len = textarea.selectionStart;
     textarea.value = textarea.value.substr( 0,len) + textarea.value.substr(textarea.selectionEnd,textarea.value.length);
     textarea.value = textarea.value.substr( 0, len ) + text + textarea.value.substr( len );
     textarea.selectionStart = len + text.length;
     textarea.selectionEnd = len + text.length;
//     textarea.setSelectionRange(len,len+text.length);
  } else insertAtCursor(textarea.text);
  textarea.focus()
}

function BBC(bbcname) {
  var textarea = getTextarea();
  var theSelection = getTextareaSelection();
  if (theSelection != '') {
    PostWrite("[" + bbcname + "]" + theSelection + "[/" + bbcname + "]");
    return;
  }
  if (bbcname == 'b') var bbcid=0;
  if (bbcname == 'i') var bbcid=1;
  if (bbcname == 'center') var bbcid=3;
  if (!bb_open[bbcid]) {
    ToAdd = "[" + bbcname + "]";
    bb_open[bbcid] = 1;
  } else {
    ToAdd = "[/" + bbcname + "]";
    bb_open[bbcid] = 0;
  }
  PostWrite(ToAdd);
}


function BBCurl() {
  var textarea = getTextarea();
  var enterTITLE = getTextareaSelection();
  var FoundErrors = '';
  var enterURL   = prompt("Please enter the URL to link to", "http://");
  if (!enterURL || enterURL == 'http://')    {
    FoundErrors += "Please enter a URL to link to";
  }
  if (FoundErrors)  {
    alert(FoundErrors);
    return;
  }
  if (enterTITLE != '')
    var ToAdd = "[url="+enterURL+"]"+enterTITLE+"[/url]";
  else var ToAdd = "[url]"+enterURL+"[/url]";
  PostWrite(ToAdd);
  textarea.focus();
}

function BBCimg() {
  var textarea = getTextarea();
  var FoundErrors = '';
  var theSelection = getTextareaSelection();
  if (theSelection == '')
    var enterURL   = prompt("Please enter the image to include","http://");
  else var enterURL = theSelection;
  if (!enterURL || enterURL == 'http://') {
    FoundErrors += "Please enter the address of the image to include";
  }
  if (FoundErrors) {
    alert(FoundErrors);
    return;
  }
  var ToAdd = "[img]"+enterURL+"[/img]";
  PostWrite(ToAdd);
  textarea.focus();
}

function checkForm() {
  var textarea = getTextarea();
  formErrors = false;
  if (textarea.value.length < 2) {
    formErrors = 'You must enter a message when posting.';
  }
  if (formErrors) {
    alert(formErrors);
    return false;
  } else {
    return true;
  }
}

function insertAtCursor(myField, myValue) {
  //IE support
  if (document.selection) {
    myField.focus();
    sel = document.selection.createRange();
    sel.text = myValue;
  }
  //MOZILLA/NETSCAPE support
  else if (myField.selectionStart || myField.selectionStart == '0') {
    var startPos = myField.selectionStart;
    var endPos = myField.selectionEnd;
    myField.value = myField.value.substring(0, startPos)
    + myValue
    + myField.value.substring(endPos, myField.value.length);
    myField.selectionStart = startPos + myValue.length;
    myField.selectionEnd = endPos + myValue.length;
  } else {
    myField.value += myValue;
  }
}

function myKeyDown(e){
  if (window.Event){
    mykey = e.which;
  }
  else{
    mykey = event.keyCode
  }
  switch (mykey)  {
    case 17: // ctrl
      ctrl_pressed = true;
      break;
    case 16: // shift
      shift_pressed = true;
      break;
    case 66: // B
      if (ctrl_pressed && shift_pressed)  {
        BBC('b');
        setClicked('b');
        ctrl_pressed = false;
        shift_pressed = false;
        break;
      }
      break;
    case 81: // Q
      if (ctrl_pressed && shift_pressed)  {
        quoteSelection();
        setClicked('quote');
        ctrl_pressed = false;
        shift_pressed = false;
        break;
      }
      break;
    case 73: // I
      if (ctrl_pressed && shift_pressed)  {
        BBC('i');
        setClicked('i');
        ctrl_pressed = false;
        shift_pressed = false;
        break;
      }
      break;
    case 76: // L
      if (ctrl_pressed && shift_pressed)  {
        BBCurl();
        ctrl_pressed = false;
        shift_pressed = false;
        break;
      }
      break;
    default:
    break;
  }
}

function myKeyUp(e){
  if (window.Event){
    mykey = e.which;
  }
  else{
    mykey = event.keyCode
  }
  if (mykey == 17)  {
    ctrl_pressed = false;
  } else if (mykey == 16)  {
    shift_pressed = false;
  }
}
